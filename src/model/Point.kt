package model

import kotlin.math.abs

class Point(val x: Int, val y: Int) {

    fun calculateTaxicabDistance(point: Point): Int {
        return abs(x - point.x) + abs(y - point.y)
    }

}
