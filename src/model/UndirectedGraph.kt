package model

/**
 * Class represent undirected graph
 */
class UndirectedGraph(val vertexCount: Int) {

    private val adjacencyMatrix: Array<IntArray> = Array(vertexCount) { IntArray(vertexCount) { -1 } }

    operator fun get(fromVertex: Int, toVertex: Int): Int {
        return adjacencyMatrix[fromVertex][toVertex]
    }

    operator fun set(fromVertex: Int, toVertex: Int, weight: Int) {
        adjacencyMatrix[fromVertex][toVertex] = weight
        adjacencyMatrix[toVertex][fromVertex] = weight
    }

    fun addEdge(undirectedEdge: UndirectedEdge) {
        adjacencyMatrix[undirectedEdge.fromVertex][undirectedEdge.toVertex] = undirectedEdge.weight
        adjacencyMatrix[undirectedEdge.toVertex][undirectedEdge.fromVertex] = undirectedEdge.weight
    }

    fun getEdge(fromVertex: Int, toVertex: Int): UndirectedEdge? {
        val weight = adjacencyMatrix[fromVertex][toVertex]
        if (weight == -1) {
            return null
        }
        return UndirectedEdge(fromVertex, toVertex, weight)
    }

    fun removeEdge(fromVertex: Int, toVertex: Int): UndirectedEdge? {
        val undirectedEdge = getEdge(fromVertex, toVertex) ?: return null
        adjacencyMatrix[fromVertex][toVertex] = -1
        adjacencyMatrix[toVertex][fromVertex] = -1
        return undirectedEdge
    }

    fun removeEdge(undirectedEdge: UndirectedEdge): UndirectedEdge? {
        return removeEdge(undirectedEdge.fromVertex, undirectedEdge.toVertex)
    }

    fun getEdges(): ArrayList<UndirectedEdge> {
        val edges = ArrayList<UndirectedEdge>()
        for (fromVertex in IntRange(0, vertexCount - 1)) {
            for (toVertex in IntRange(fromVertex + 1, vertexCount - 1)) {
                val weight = adjacencyMatrix[fromVertex][toVertex]
                if (weight == -1) {
                    continue
                }
                edges.add(UndirectedEdge(fromVertex, toVertex, weight))
            }
        }
        return edges
    }

    fun getMinWeightEdge(): UndirectedEdge? {
        var minWeight = -1
        var minWeightFromVertex = -1
        var minWeightToVertex = -1

        for (fromVertex in IntRange(0, vertexCount - 1)) {
            for (toVertex in IntRange(fromVertex + 1, vertexCount - 1)) {
                val weight = adjacencyMatrix[fromVertex][toVertex]
                if (weight == -1) {
                    continue
                }
                if (minWeight == -1 || minWeight > weight) {
                    minWeight = weight
                    minWeightFromVertex = fromVertex
                    minWeightToVertex = toVertex
                }
            }
        }

        if (minWeight == -1) {
            return null
        }

        return UndirectedEdge(minWeightFromVertex, minWeightToVertex, minWeight)
    }

    fun resetWeights() {
        for (fromVertex in IntRange(0, vertexCount - 1)) {
            for (toVertex in IntRange(0, vertexCount - 1)) {
                adjacencyMatrix[fromVertex][toVertex] = -1
            }
        }
    }

}
