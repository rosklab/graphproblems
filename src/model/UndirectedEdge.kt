package model

import java.util.*

/**
 * Class represent edge of undirected graph
 */
// TODO: 22.12.2019 Maybe add Comparable interface
class UndirectedEdge(val fromVertex: Int, val toVertex: Int, val weight: Int) {

    override operator fun equals(other: Any?): Boolean {
        if (other === null || other !is UndirectedEdge) {
            return false
        }

        if (this === other) {
            return true
        }

        return weight == other.weight &&
                ((fromVertex == other.fromVertex && toVertex == other.toVertex) ||
                (fromVertex == other.toVertex && toVertex == other.fromVertex))
    }

    override fun hashCode(): Int {
        return Objects.hash(fromVertex, toVertex, weight)
    }

    override fun toString(): String {
        return "UndirectedEdge {fromVertex = $fromVertex, toVertex = $toVertex, weight = $weight}"
    }

}
