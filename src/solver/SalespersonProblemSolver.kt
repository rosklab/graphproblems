package solver

import model.UndirectedEdge

interface SalespersonProblemSolver {

    fun findSolution(): Pair<ArrayList<UndirectedEdge>, ArrayList<UndirectedEdge>>

    // TODO: 22.12.2019 Add initialize method

}
