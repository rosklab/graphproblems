package solver

import model.UndirectedGraph

interface MaximumSubgraphFinder {

    fun findUndirectedSubgraph(): UndirectedGraph

}
