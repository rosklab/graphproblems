package solver.implementation

import model.UndirectedEdge
import model.UndirectedGraph
import solver.MaximumSubgraphFinder
import util.writeUndirectedSubgraph
import kotlin.random.Random

class LocalMaximumSubgraphFinder(private val undirectedGraph: UndirectedGraph,
                                 private val limit: Int,
                                 private val outputFileName: String): MaximumSubgraphFinder {

    private val undirectedSubgraph: UndirectedGraph
    private val sortedGraphUndirectedEdges = undirectedGraph.getEdges().sortedByDescending { it.weight }
    private val median = sortedGraphUndirectedEdges[sortedGraphUndirectedEdges.size / 2 + 1].weight

    init {
        val greedyMaximumSubgraphFinder = GreedyMaximumSubgraphFinder(undirectedGraph, limit)
        undirectedSubgraph = greedyMaximumSubgraphFinder.findUndirectedSubgraph()
        println("Greedy total weight: ${undirectedSubgraph.getEdges().sumBy { it.weight }}")
        println("Median: $median")
    }

    override fun findUndirectedSubgraph(): UndirectedGraph {
        for(iteration in IntRange(0, undirectedSubgraph.vertexCount - 1)) {
            println("Local search iteration $iteration")
            // val minWeightEdge = undirectedSubgraph.getMinWeightEdge() ?: break
            var subgraphModified = false
            val sortedUndirectedEdge = undirectedSubgraph.getEdges().sortedBy { it.weight }
            for (indexedUndirectedEdge in sortedUndirectedEdge.withIndex()) {
                val undirectedEdge = indexedUndirectedEdge.value
                undirectedSubgraph.removeEdge(undirectedEdge)
                var maxWeight = undirectedEdge.weight
                var suitableUndirectedEdge: UndirectedEdge? = null
                var checkedUndirectedEdge: UndirectedEdge?

                for (vertex in IntRange(0, undirectedSubgraph.vertexCount - 1)) {
                    if (undirectedEdge.toVertex != vertex) {
                        checkedUndirectedEdge = checkSuitable(undirectedEdge.fromVertex, vertex, maxWeight)
                        if (checkedUndirectedEdge != null) {
                            suitableUndirectedEdge = checkedUndirectedEdge
                            maxWeight = checkedUndirectedEdge.weight
                        }
                    }

                    if (undirectedEdge.fromVertex != vertex) {
                        checkedUndirectedEdge = checkSuitable(undirectedEdge.toVertex, vertex, maxWeight)
                        if (checkedUndirectedEdge != null) {
                            suitableUndirectedEdge = checkedUndirectedEdge
                            maxWeight = checkedUndirectedEdge.weight
                        }
                    }
                }

                if (indexedUndirectedEdge.index % 100 == 0) {
                    // TODO: 26.12.2019 Add detail check of error
                    try {
                        writeUndirectedSubgraph(undirectedSubgraph, outputFileName)
                    } catch (e: Exception) {
                        println("Can't write information")
                    }
                }

                if (suitableUndirectedEdge == null) {
                    undirectedSubgraph.addEdge(undirectedEdge)
                    continue
                }

                println("Sum increase on ${suitableUndirectedEdge.weight - undirectedEdge.weight}")
                undirectedSubgraph.addEdge(suitableUndirectedEdge)
                subgraphModified = true
            }

            subgraphModified = addNewEdges(subgraphModified)

            if (!subgraphModified) {
                break
            }

            // TODO: 26.12.2019 Add detail check of error
            try {
                writeUndirectedSubgraph(undirectedSubgraph, outputFileName)
            } catch (e: Exception) {
                println("Can't write information")
            }
        }
        return undirectedSubgraph
    }

    private fun addNewEdges(subgraphModified: Boolean): Boolean {
        var edgesAdded = false

        for (undirectedEdge in sortedGraphUndirectedEdges) {
            if (undirectedSubgraph[undirectedEdge.fromVertex, undirectedEdge.toVertex] != -1) {
                continue
            }

            val visitedVertices = ArrayList<Int>()
            undirectedSubgraph.addEdge(undirectedEdge)
            visitedVertices.add(undirectedEdge.fromVertex)
            // Would add edge if graph not modified on last iteration
            if (existCycle(undirectedEdge.fromVertex, undirectedEdge.fromVertex, visitedVertices, 1) ||
                    (subgraphModified && Random.nextFloat() < median.toFloat() / undirectedEdge.weight)) {
                undirectedSubgraph.removeEdge(undirectedEdge)
                continue
            }
            edgesAdded = true
            println("New edge added, weight: ${undirectedEdge.weight}")
        }

        return edgesAdded || subgraphModified
    }

    private fun checkSuitable(fromVertex: Int, toVertex: Int, maxWeight: Int): UndirectedEdge? {
        if (undirectedSubgraph[fromVertex, toVertex] != -1) {
            return null
        }

        if (undirectedGraph[fromVertex, toVertex] == -1) {
            return null
        }

        var suitableUndirectedEdge: UndirectedEdge? = null
        val undirectedEdge = undirectedGraph.getEdge(fromVertex, toVertex) ?: return null
        val visitedVertices = ArrayList<Int>()
        undirectedSubgraph.addEdge(undirectedEdge)
        visitedVertices.add(undirectedEdge.fromVertex)
        // TODO: 26.12.2019 Check work with increase maxWeight
        if (maxWeight * 1.15 < undirectedEdge.weight &&
            !existCycle(undirectedEdge.fromVertex, undirectedEdge.fromVertex, visitedVertices, 1)) {
            println("Find suitable edge")
            suitableUndirectedEdge = undirectedEdge
        }
        undirectedSubgraph.removeEdge(undirectedEdge)
        return suitableUndirectedEdge
    }

    // TODO: 26.12.2019 Move block to general class
    private fun existCycle(firstVertex: Int, fromVertex: Int, visitedVertices: ArrayList<Int>, depth: Int): Boolean {
        for (toVertex in IntRange(0, undirectedSubgraph.vertexCount - 1)) {
            val weight = undirectedSubgraph[fromVertex, toVertex]
            // Check edge between vertices
            if (weight == -1) {
                continue
            }

            if (depth > 2 && firstVertex == toVertex) {
                return true
            }

            // Vertex is visited or not found cycle for limit depth
            if (visitedVertices.contains(toVertex) || depth == limit) {
                continue
            }

            visitedVertices.add(toVertex)
            if (existCycle(firstVertex, toVertex, visitedVertices, depth + 1)) {
                return true
            }
            visitedVertices.remove(toVertex)
        }
        return false
    }

}
