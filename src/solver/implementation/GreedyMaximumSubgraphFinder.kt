package solver.implementation

import model.UndirectedEdge
import model.UndirectedGraph
import solver.MaximumSubgraphFinder
import kotlin.random.Random

class GreedyMaximumSubgraphFinder(undirectedGraph: UndirectedGraph, private val limit: Int): MaximumSubgraphFinder {

    private val undirectedSubgraph = UndirectedGraph(undirectedGraph.vertexCount)
    private val sortedSuitableUndirectedEdges: ArrayList<UndirectedEdge>
    private val verticesWeight = IntArray(undirectedGraph.vertexCount)
    private val usedVertices = BooleanArray(undirectedGraph.vertexCount)

    init {
        val sortedUndirectedEdges = undirectedGraph.getEdges().sortedBy { it.weight }
        sortedSuitableUndirectedEdges = ArrayList(sortedUndirectedEdges)
        for (undirectedEdge in sortedUndirectedEdges) {
            verticesWeight[undirectedEdge.fromVertex] += undirectedEdge.weight
            verticesWeight[undirectedEdge.toVertex] += undirectedEdge.weight
        }
    }

    override fun findUndirectedSubgraph(): UndirectedGraph {
        var iteration = 0
        while (sortedSuitableUndirectedEdges.size > 0) {
        // repeat (undirectedSubgraph.vertexCount * 20) {
            println("New edge iteration $iteration")
            val undirectedEdge = sortedSuitableUndirectedEdges.removeAt(findNextSuitableIndex())
            undirectedSubgraph.addEdge(undirectedEdge)

            val visitedVertices = ArrayList<Int>()
            visitedVertices.add(undirectedEdge.fromVertex)
            // Since the edge must fully enter the cycle, it is enough to check one of the vertices
            if (existCycle(undirectedEdge.fromVertex, undirectedEdge.fromVertex, visitedVertices, 1)) {
                undirectedSubgraph.removeEdge(undirectedEdge)
            } else {
                usedVertices[undirectedEdge.fromVertex] = true
                usedVertices[undirectedEdge.toVertex] = true
            }

            // Decrease weight of vertex after removing edge
            verticesWeight[undirectedEdge.fromVertex] -= undirectedEdge.weight
            verticesWeight[undirectedEdge.toVertex] -= undirectedEdge.weight
            iteration++
        }
        return undirectedSubgraph
    }

    private fun findNextSuitableIndex(): Int {
        // TODO: 26.12.2019 Change finding of suitable index
        // TODO: 26.12.2019 Create special class for random solutions
        // Neighboring edge index choose
        // for (index in IntRange(0, sortedSuitableUndirectedEdges.size - 1).reversed()) {
        //     val undirectedEdge = sortedSuitableUndirectedEdges[index]
        //     if (usedVertices[undirectedEdge.fromVertex] || usedVertices[undirectedEdge.toVertex]) {
        //         return index
        //     }
        // }

        // TODO: 26.12.2019 Add random if needed
        // Random edge index choose
        if (Random.nextFloat() < 0.7) {
            return Random.nextInt(sortedSuitableUndirectedEdges.size)
        }

        var maxWeightVertex = 0
        var maxWeight = verticesWeight[0]
        var maxWeightUsedVertex = -1
        var usedMaxWeight = -1
        for (vertex in IntRange(0, verticesWeight.size - 1)) {
            val weight = verticesWeight[vertex]
            if (maxWeight < weight) {
                maxWeightVertex = vertex
                maxWeight = weight
            }

            // Don't check for unusedMaxWeight == -1 because weight is positive
            if (usedVertices[vertex] && usedMaxWeight < weight) {
                maxWeightUsedVertex = vertex
                usedMaxWeight = weight
            }
        }

        var suitableVertex = maxWeightVertex
        if (maxWeightUsedVertex != -1) {
            suitableVertex = maxWeightUsedVertex
        }

        for (index in IntRange(0, sortedSuitableUndirectedEdges.size - 1).reversed()) {
            val undirectedEdge = sortedSuitableUndirectedEdges[index]
            if (undirectedEdge.fromVertex == suitableVertex || undirectedEdge.toVertex == suitableVertex) {
                return index
            }
        }

        return sortedSuitableUndirectedEdges.size - 1
    }

    // TODO: 26.12.2019 Move block to general class
    private fun existCycle(firstVertex: Int, fromVertex: Int, visitedVertices: ArrayList<Int>, depth: Int): Boolean {
        for (toVertex in IntRange(0, undirectedSubgraph.vertexCount - 1)) {
            val weight = undirectedSubgraph[fromVertex, toVertex]
            // Check edge between vertices
            if (weight == -1) {
                continue
            }

            if (depth > 2 && firstVertex == toVertex) {
                return true
            }

            // Vertex is visited or not found cycle for limit depth
            if (visitedVertices.contains(toVertex) || depth == limit) {
                continue
            }

            visitedVertices.add(toVertex)
            if (existCycle(firstVertex, toVertex, visitedVertices, depth + 1)) {
                return true
            }
            visitedVertices.remove(toVertex)
        }
        return false
    }

}
