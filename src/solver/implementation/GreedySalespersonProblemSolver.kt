package solver.implementation

import model.UndirectedEdge
import model.UndirectedGraph
import solver.SalespersonProblemSolver

class GreedySalespersonProblemSolver(private val undirectedGraph: UndirectedGraph): SalespersonProblemSolver {

    private val edgePath: ArrayList<UndirectedEdge> = ArrayList(undirectedGraph.vertexCount)
    private val visitedVertices: ArrayList<Int> = ArrayList(undirectedGraph.vertexCount)
    private val blockedEdges: ArrayList<UndirectedEdge> = ArrayList()
    private var firstVertex: Int = 0

    override fun findSolution(): Pair<ArrayList<UndirectedEdge>, ArrayList<UndirectedEdge>> {
        // Find path from first vertex
        // TODO: 22.12.2019 Maybe need check for vertices count
        var (firstBestEdgePath, secondBestEdgePath) = findPaths(0)
        var firstBestEdgePathWeight = firstBestEdgePath.sumBy { it.weight }
        var secondBestEdgePathWeight = secondBestEdgePath.sumBy { it.weight }
        var bestEdgePathsWeight = firstBestEdgePathWeight + secondBestEdgePathWeight
        printInformation(firstBestEdgePathWeight, secondBestEdgePathWeight)
        for (vertex in IntRange(1, undirectedGraph.vertexCount - 1)) {
            println("Current vertex: $vertex")
            // Clear blocked edges for finding solution
            blockedEdges.clear()
            val (firstEdgePath, secondEdgePath) = findPaths(vertex)
            val firstEdgePathWeight = firstEdgePath.sumBy { it.weight }
            val secondEdgePathWeight = secondEdgePath.sumBy { it.weight }
            val edgePathsWeight = firstEdgePathWeight + secondEdgePathWeight
            if (bestEdgePathsWeight < edgePathsWeight) {
                continue
            }
            if (bestEdgePathsWeight > edgePathsWeight ||
                (firstBestEdgePathWeight.coerceAtLeast(secondBestEdgePathWeight) >
                firstEdgePathWeight.coerceAtLeast(secondEdgePathWeight))) {
                firstBestEdgePath = firstEdgePath
                secondBestEdgePath = secondEdgePath
                firstBestEdgePathWeight = firstEdgePathWeight
                secondBestEdgePathWeight = secondEdgePathWeight
                bestEdgePathsWeight = edgePathsWeight
                printInformation(firstBestEdgePathWeight, secondBestEdgePathWeight)
            }
        }
        return Pair(firstBestEdgePath, secondBestEdgePath)
    }

    private fun findPaths(vertex: Int): Pair<ArrayList<UndirectedEdge>, ArrayList<UndirectedEdge>> {
        firstVertex = vertex
        initializeConditionsForPath(firstVertex)
        findGreedyPath(firstVertex)
        val firstEdgePath = ArrayList(edgePath)

        initializeConditionsForPath(firstVertex)
        findGreedyPath(firstVertex)
        val secondEdgePath = ArrayList(edgePath)
        return Pair(firstEdgePath, secondEdgePath)
    }

    private fun findGreedyPath(fromVertex: Int): Boolean {
        if (visitedVertices.count() == undirectedGraph.vertexCount) {
            return checkLastEdge(fromVertex)
        }

        val unsuitableVertices = ArrayList<Int>()
        while (true) {
            val undirectedEdge = findEdgeWithMinWeight(fromVertex, unsuitableVertices) ?: return false
            edgePath.add(undirectedEdge)
            blockedEdges.add(undirectedEdge)
            // Method findEdgeWithMinWeight write in toVertex in UndirectedEdge added vertex
            visitedVertices.add(undirectedEdge.toVertex)
            unsuitableVertices.add(undirectedEdge.toVertex)
            if (findGreedyPath(undirectedEdge.toVertex)) {
                break
            }

            edgePath.remove(undirectedEdge)
            blockedEdges.remove(undirectedEdge)
            visitedVertices.remove(undirectedEdge.toVertex)
        }

        return true
    }

    private fun checkLastEdge(fromVertex: Int): Boolean {
        val undirectedEdge = undirectedGraph.getEdge(fromVertex, firstVertex) ?: return false
        if (blockedEdges.contains(undirectedEdge)) {
            return false
        }

        edgePath.add(undirectedEdge)
        blockedEdges.add(undirectedEdge)
        return true
    }

    private fun findEdgeWithMinWeight(fromVertex: Int, unsuitableVertices: ArrayList<Int>): UndirectedEdge? {
        val blockedVertices = collectBlockedVertices(fromVertex)
        // Set -1 default value for using Int instead Int?
        var minWeight = -1
        var toVertex = -1
        for (vertex in IntRange(0, undirectedGraph.vertexCount - 1)) {
            if (visitedVertices.contains(vertex) ||
                blockedVertices.contains(vertex) ||
                unsuitableVertices.contains(vertex)) {
                continue
            }

            val weight = undirectedGraph[fromVertex, vertex]
            if (minWeight == -1 || minWeight > weight) {
                minWeight = weight
                toVertex = vertex
            }
        }

        if (toVertex == -1) {
            return null
        }

        return undirectedGraph.getEdge(fromVertex, toVertex)
    }

    private fun collectBlockedVertices(fromVertex: Int): ArrayList<Int> {
        val blockedVertices = ArrayList<Int>()
        for (undirectedEdge in blockedEdges) {
            if (undirectedEdge.fromVertex == fromVertex) {
                blockedVertices.add(undirectedEdge.toVertex)
                continue
            }
            if (undirectedEdge.toVertex == fromVertex) {
                blockedVertices.add(undirectedEdge.fromVertex)
            }
        }
        return blockedVertices
    }

    private fun initializeConditionsForPath(vertex: Int) {
        edgePath.clear()
        visitedVertices.clear()
        visitedVertices.add(vertex)
    }

    private fun printInformation(firstEdgePathWeight: Int, secondEdgePathWeight: Int) {
        println("First path weight: $firstEdgePathWeight")
        println("Second path weight: $secondEdgePathWeight")
        println("Total weight: ${firstEdgePathWeight + secondEdgePathWeight}")
    }

}
