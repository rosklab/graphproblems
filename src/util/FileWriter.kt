package util

import model.UndirectedEdge
import model.UndirectedGraph
import java.io.File

fun writeUndirectedEdgePath(undirectedEdgePath: List<UndirectedEdge>, fileName: String) {
    val transformUndirectedEdgePath = undirectedEdgePath.map {
        if (it.fromVertex > it.toVertex) {
            return@map UndirectedEdge(it.toVertex, it.fromVertex, it.weight)
        }
        return@map it
    }
    val sortedUndirectedEdgePath = transformUndirectedEdgePath.sortedWith(compareBy({ it.fromVertex }, { it.toVertex }))
    val vertices = LinkedHashSet<Int>()
    for (undirectedEdge in sortedUndirectedEdgePath) {
        vertices.add(undirectedEdge.fromVertex)
        vertices.add(undirectedEdge.toVertex)
    }

    val printWriter = File(fileName).printWriter()
    printWriter.use {
        it.println("c Example of DIMACS format file")
        it.println("p edge ${vertices.size} ${sortedUndirectedEdgePath.size}")
        for (undirectedEdge in sortedUndirectedEdgePath) {
            it.println("e ${undirectedEdge.fromVertex + 1} ${undirectedEdge.toVertex + 1}")
        }
    }
}

fun writeUndirectedSubgraph(undirectedSubgraph: UndirectedGraph, fileName: String) {
    val undirectedSubgraphEdges = undirectedSubgraph.getEdges()
    val totalWeight = undirectedSubgraphEdges.sumBy { it.weight }

    val printWriter = File(fileName).printWriter()
    printWriter.use {
        it.println("c Вес подграфа = $totalWeight")
        it.println("p edge ${undirectedSubgraph.vertexCount} ${undirectedSubgraphEdges.size}")
        for (undirectedEdge in undirectedSubgraphEdges) {
            it.println("e ${undirectedEdge.fromVertex + 1} ${undirectedEdge.toVertex + 1}")
        }
    }
}
