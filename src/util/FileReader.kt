package util

import model.Point
import model.UndirectedGraph
import java.io.File

fun readUndirectedGraph(fileName: String): UndirectedGraph {
    val lines = File(fileName).readLines()
    // Remove count line from conditions
    val conditions = lines.drop(1)
    val pointList = parsePointsInformation(conditions)
    return createGraph(pointList)
}

private fun parsePointsInformation(conditions: List<String>): List<Point> {
    val pointList = ArrayList<Point>()
    for (condition in conditions) {
        val conditionParts = condition.split('\t')
        pointList.add(Point(conditionParts[1].toInt(), conditionParts[2].toInt()))
    }
    return pointList
}

private fun createGraph(pointList: List<Point>): UndirectedGraph {
    val vertexCount = pointList.size
    val undirectedGraph = UndirectedGraph(vertexCount)
    for (i in IntRange(0, vertexCount - 1)) {
        val firstPoint = pointList[i]
        for (j in IntRange(i + 1, vertexCount - 1)) {
            val secondPoint = pointList[j]
            undirectedGraph[i, j] = firstPoint.calculateTaxicabDistance(secondPoint)
        }
    }
    return undirectedGraph
}
