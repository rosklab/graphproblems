import model.UndirectedEdge
import model.UndirectedGraph
import solver.implementation.GreedySalespersonProblemSolver
import solver.implementation.LocalMaximumSubgraphFinder
import util.readUndirectedGraph
import util.writeUndirectedEdgePath
import util.writeUndirectedSubgraph

const val inputFilePath = "D:\\Projects\\AlgorithmsProjects\\GraphProblems\\data\\input"
const val outputFilePath = "D:\\Projects\\AlgorithmsProjects\\GraphProblems\\data\\output"

fun main(args: Array<String>) {
    if (args.size < 2) {
        println("Invalid number of arguments")
        return
    }

    println("Program start")
    val fileName = "Taxicab_${args[1]}.txt"
    when (args[0]) {
        "2PSP" -> {
            solveSalespersonProblem("$inputFilePath\\salesperson\\$fileName",
                                    "$outputFilePath\\salesperson\\$fileName")
        }
        "C3C4" -> {
            findMaximumSubgraph("$inputFilePath\\subgraph\\$fileName",
                                "$outputFilePath\\subgraph\\$fileName")
        }
    }
    println("Program end")
}

private fun solveSalespersonProblem(inputFileName: String, outputFileName: String) {
    val undirectedGraph = readUndirectedGraph(inputFileName)
    val salespersonProblemSolver = GreedySalespersonProblemSolver(undirectedGraph)
    val (firstPath, secondPath) = salespersonProblemSolver.findSolution()
    printSalespersonProblemResult(firstPath, secondPath)
    // TODO: 24.12.2019 Customize writeSalespersonProblemResult
    writeSalespersonProblemResult(firstPath, /*secondPath,*/ outputFileName)
}

private fun findMaximumSubgraph(inputFileName: String, outputFileName: String) {
    val undirectedGraph = readUndirectedGraph(inputFileName)
    // val maximumSubgraphFinder = GreedyMaximumSubgraphFinder(undirectedGraph, 4)
    // TODO: 26.12.2019 Decide of outputFileName in constructor
    val maximumSubgraphFinder = LocalMaximumSubgraphFinder(undirectedGraph, 4, outputFileName)
    val undirectedSubgraph = maximumSubgraphFinder.findUndirectedSubgraph()
    printMaximumSubgraphResult(undirectedSubgraph)
    writeMaximumSubgraphResult(undirectedSubgraph, outputFileName)
}

private fun printGraph(graph: UndirectedGraph) {
    val vertexRange = IntRange(0, graph.vertexCount - 1)
    for (fromVertex in vertexRange) {
        for (toVertex in vertexRange) {
            print(graph[fromVertex, toVertex].toString().padStart(2, ' ') + " ")
        }
        println()
    }
}

private fun printMaximumSubgraphResult(undirectedSubgraph: UndirectedGraph) {
    val edges = undirectedSubgraph.getEdges()
    println("Subgraph result")
    println("Edge count: ${edges.size}")
    println("Total weight: ${edges.sumBy { it.weight }}")
}

private fun writeMaximumSubgraphResult(undirectedSubgraph: UndirectedGraph, outputFileName: String) {
    writeUndirectedSubgraph(undirectedSubgraph, outputFileName)
}

private fun printSalespersonProblemResult(firstPath: ArrayList<UndirectedEdge>, secondPath: ArrayList<UndirectedEdge>) {
    val firstPathWeight = firstPath.sumBy { it.weight }
    val secondPathWeight = secondPath.sumBy { it.weight }
    println("First path weight: $firstPathWeight")
    println("Second path weight: $secondPathWeight")
    println("Total weight: ${firstPathWeight + secondPathWeight}")
    printPath(firstPath, "firstPath")
    printPath(secondPath, "secondPath")
}

private fun writeSalespersonProblemResult(
    firstPath: ArrayList<UndirectedEdge>,
    // TODO: 24.12.2019 Add secondPath processing
    // secondPath: ArrayList<UndirectedEdge>,
    outputFileName: String
) {
    writeUndirectedEdgePath(firstPath, outputFileName)
    // TODO: 24.12.2019 Add logic for two paths
}

private fun printPath(edgePath: ArrayList<UndirectedEdge>, text: String) {
    print("$text: ")
    for (undirectedEdge in edgePath) {
        print("${undirectedEdge.fromVertex + 1} ")
    }
    println()
}
